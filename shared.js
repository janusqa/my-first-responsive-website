const backdrop = document.querySelector('.backdrop');
const modal = document.querySelector('.modal');
const planButtons = document.querySelectorAll('.plan button');
const modalNoButton = document.querySelector('button.modal__action--negative');
const toggleButton = document.querySelector('.toggle-button');
const mobileNav = document.querySelector('.mobile-nav');

planButtons.forEach(function(button) {
  button.addEventListener('click', function() {
    modal.classList.add('open');
    backdrop.classList.add('open');
  });
});

backdrop.addEventListener('click', function() {
  if (modal) {
    modal.classList.remove('open');
  }
  backdrop.classList.remove('open');
  mobileNav.classList.remove('open');
});

if (modalNoButton) {
  modalNoButton.addEventListener('click', function() {
    modal.classList.remove('open');
    backdrop.classList.remove('open');
  });
}

toggleButton.addEventListener('click', function() {
  mobileNav.classList.add('open');
  backdrop.classList.add('open');
});
